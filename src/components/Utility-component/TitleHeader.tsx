
type TitleHeaderProps = {
	title: string;
	color?: string;
}

export const TitleHeader = (props: TitleHeaderProps) => {

	const { title, color} = props;

	return (
		<>
			<h1 style={{color: color || 'blue'}}>{title}</h1>
		</>
	)
}
