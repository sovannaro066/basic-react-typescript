import React from "react";
import { ComponentC } from "./ComponentC";
import { ComponentE } from "./ComponentE";
import { TitleHeader } from "../../Utility-component/TitleHeader";

export const UserContext = React.createContext('');
export const ChannelContext = React.createContext('');

export const ComponentMain = () => {

	return (
		<>
			<TitleHeader title="useContext Easy Understand"/>
			<UserContext.Provider value={'Naro'}>
				<ChannelContext.Provider value={'Codevolution'}>
					<ComponentC />
					<ComponentE />
				</ChannelContext.Provider>
			</UserContext.Provider>
		</>
	)
}
