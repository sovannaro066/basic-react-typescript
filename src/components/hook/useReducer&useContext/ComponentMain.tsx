import {createContext, Dispatch, useReducer} from "react";
import {ComponentA} from "./ComponentA";
import {ComponentB} from "./ComponentB";
import {ComponentC} from "./ComponentC";
import { TitleHeader } from "../../Utility-component/TitleHeader";

const initialState = 0

type Action = {
	type: 'increment' | 'decrement' | 'reset'
}

type DispatchType = Dispatch<Action>

export const CountContext = createContext<{
	count: number,
	dispatch: DispatchType
}>
({
	count: 0,
	dispatch: () => null
})

const reducer = (state: number, action: Action) => {
	switch (action.type) {
		case 'increment':
			return state + 1
		case 'decrement':
			return state - 1
		case 'reset':
			return initialState
		default:
			return state
	}
}

export const ReducerAndContext = () => {

	const [count, dispatch] = useReducer(reducer, initialState)

	return (
		<>
			<TitleHeader title="useReducer & useContext"/>
			<CountContext.Provider value={{count, dispatch}}>
				Global Count: {count}
				<ComponentA/>
				<ComponentB/>
				<ComponentC/>
			</CountContext.Provider>
		</>

	)
}
