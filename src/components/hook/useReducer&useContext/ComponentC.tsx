import {useContext} from "react";
import {CountContext} from "./ComponentMain";

export const ComponentC = () => {

	const countContext = useContext(CountContext)

	return (
		<>
			<h3>Component C</h3>
			<div>
				Count: {countContext.count}
			</div>
			<button onClick={() => countContext.dispatch({type: 'increment'})}>Increment</button>
			<button onClick={() => countContext.dispatch({type: 'decrement'})}>Decrement</button>
			<button onClick={() => countContext.dispatch({type: 'reset'})}>Reset</button>
		</>
	)
}
