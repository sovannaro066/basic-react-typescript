import { useCallback, useState } from "react";
import { Count } from "./Count";
import { Title } from "./Title";
import { Button } from "./Button";

export const ParentComponent = () => {

	const [age, setAge] = useState(20)
	const [salary, setSalary] = useState(10000)

	const incrementAge = useCallback(() => {
		setAge(age + 1)
	}, [age])

	const incrementSalary = useCallback(() => {
		setSalary(salary + 1000)
	}, [salary])

	return (
		<>
			<Title/>
			<div>
				<Count text={"Age"} count={age}/>
				<Button handleClick={incrementAge}>Increment Age</Button>
			</div>
			<div>
				<Count text={"Salary"} count={salary}/>
				<Button handleClick={incrementSalary}>Increment Salary</Button>
			</div>
		</>
	)
}
