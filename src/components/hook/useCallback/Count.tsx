import { memo } from "react";

type CountProps = {
	text: string;
	count: number;
}

export const Count = memo(({ text, count }: CountProps) => {
	console.log(`Rendering ${text}`)
	return (
		<div>
			{text} - {count}
		</div>
	)
})

