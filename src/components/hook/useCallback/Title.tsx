import { TitleHeader } from "../../Utility-component/TitleHeader";
import { memo } from "react";

// use memo to prevent unnecessary re-rendering
export const Title = memo(() => {
	console.log('Rendering Title')
	return (
		<>
			<TitleHeader title={'useCallback'}/>
		</>
	)
})
