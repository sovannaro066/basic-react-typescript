import { memo } from "react";

type ButtonProps = {
	handleClick: () => void;
	children: string;
}

export const Button = memo(({ handleClick, children}: ButtonProps) => {
	console.log(`Rendering button - ${children}`)
	return (
		<>
			<button onClick={handleClick}>{children}</button>
		</>
	)
})
