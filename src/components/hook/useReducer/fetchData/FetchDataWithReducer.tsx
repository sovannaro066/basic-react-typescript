import { TitleHeader } from "../../../Utility-component/TitleHeader";
import { useEffect, useReducer } from "react";

type Todo = {
	id: number;
	title: string;
}

const initialState = {
	loading: true,
	error: '',
	post: {} as Todo
}

type State = typeof initialState

type Action =
	| { type: 'FETCH_SUCCESS', payload: Todo }
	| { type: 'FETCH_ERROR', payload: string }

const reducer = (state: State, action: Action) => {
	switch (action.type) {
		case 'FETCH_SUCCESS':
			return {
				loading: false,
				error: '',
				post: action.payload
			}
		case 'FETCH_ERROR':
			return {
				loading: false,
				error: action.payload,
				post: {} as Todo
			}
		default:
			return state
	}
}

export const FetchDataWithReducer = () => {

	const [state, dispatch] = useReducer(reducer, initialState)

	useEffect(() => {
		fetch('https://jsonplaceholder.typicode.com/todos/1')
			.then(response => {
				return response.json()
			})
			.then(data => {
				dispatch({type: 'FETCH_SUCCESS', payload: data})
			})
			.catch(error => {
				dispatch({type: 'FETCH_ERROR', payload: 'Something went wrong!'})
			})
	}, []);

	return (
		<>
			<TitleHeader title={'Fetch Data with useReducer'}/>
			{state.loading ? 'Loading...' : <div>{state.post.id}. {state.post.title}</div>}
		</>
	)
}
