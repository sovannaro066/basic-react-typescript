import { TitleHeader } from "../../../Utility-component/TitleHeader";
import { useEffect, useState } from "react";

type Todo = {
	id: number;
	title: string;
}

export const FetchDataNoReducer = () => {

	const [loading, setLoading] = useState(true)
	const [error, setError] = useState('')
	const [post, setPost] = useState<Todo>({} as Todo)

	useEffect(() => {
		fetch('https://jsonplaceholder.typicode.com/todos/1')
			.then(response => {
				setLoading(false)
				return response.json()
			})
			.then(data => {
				setPost(data)
				setError('')
			})
			.catch(error => {
				setLoading(false)
				setPost({} as Todo)
				setError('Something went wrong!')
			})
	}, []);

	return (
		<>
			<TitleHeader title={'Fetch Data Without useReducer'}/>
			{loading ? 'Loading...' : <div>{post.id}. {post.title}</div>}
			{error ? error : null}
		</>
	)
}
