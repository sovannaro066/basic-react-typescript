import { useReducer } from "react";

const initialState = 0

const reducer = (state: number, action: string) => {
	switch (action) {
		case 'increment':
			return state + 1
		case 'decrement':
			return state - 1
		case 'reset':
			return initialState
		default:
			return state
	}
}

export const CounterThree = () => {

	const [count, dispatch] = useReducer(reducer, initialState)
	const [countTwo, dispatchTwo] = useReducer(reducer, initialState)

	return (
		<>
			<h3> Multiple Reducer </h3>
			<div>
				<div>Count: {count}</div>
				<button onClick={() => dispatch('increment')}>Increment</button>
				<button onClick={() => dispatch('decrement')}>Decrement</button>
				<button onClick={() => dispatch('reset')}>Reset</button>
				<div>Count Two: {countTwo}</div>
				<button onClick={() => dispatchTwo('increment')}>Increment</button>
				<button onClick={() => dispatchTwo('decrement')}>Decrement</button>
				<button onClick={() => dispatchTwo('reset')}>Reset</button>
			</div>
		</>
	)
}
