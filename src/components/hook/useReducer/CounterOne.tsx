import { TitleHeader } from "../../Utility-component/TitleHeader";
import { useReducer } from "react";

const initialState = 0;

const reducer = (state: number, action: string) => {
	switch (action) {
		case 'increment':
			return state + 1;
		case 'decrement':
			return state - 1;
		case 'reset':
			return initialState;
		default:
			return state;
	}
}

export const CounterOne = () => {

	const [count, dispatch] = useReducer(reducer, initialState);

	return (
		<>
			<TitleHeader title="useReducer" />
			<div>
				<div>Count: {count}</div>
				<button onClick={() => dispatch('increment')}>Increment</button>
				<button onClick={() => dispatch('decrement')}>Decrement</button>
				<button onClick={() => dispatch('reset')}>Reset</button>
			</div>
		</>
	)
}
