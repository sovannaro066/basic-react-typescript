import useInput from "./useInput";
import React from "react";

const UseForm = () => {
    const [firstName, bindFirstName, resetFirstName] = useInput("");
    const [lastName, bindLastName, resetLastName] = useInput("");

    const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        alert(`Hello ${firstName} ${lastName}`);
        resetFirstName();
        resetLastName();
    };

    return (
        <div>
			<h2>Use Form Custom Input</h2>
            <form onSubmit={submitHandler}>
                <div>
                    <label>First Name</label>
                    <input type="text" {...bindFirstName} />
                </div>
                <div>
                    <label>Last Name</label>
                    <input type="text" {...bindLastName} />
                </div>
                <button>Submit</button>
            </form>
        </div>
    );
}

export default UseForm;
