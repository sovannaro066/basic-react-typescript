import { useState, ChangeEvent } from "react";

type BindType = {
    value: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
};

function useInput(initialValue: string): [string, BindType, () => void] {

    const [value, setValue] = useState<string>(initialValue);

    const reset = () => {
        setValue(initialValue);
    };

    const bind: BindType = {
        value,
        onChange: (e: ChangeEvent<HTMLInputElement>) => {
            setValue(e.target.value);
        }
    };

    return [value, bind, reset];
}

export default useInput;
