import { useState } from "react";
import useDocumentTitle from "./useDocumentTitle";
import { TitleHeader } from "../../../Utility-component/TitleHeader";

export const DocTitleOne = () => {

	const [count, setCount] = useState(0)

	useDocumentTitle(count)

	return (
		<>
			<TitleHeader title={'Custom Hook'} />
			<button onClick={() => setCount(count + 1)}>Count - {count}</button>
		</>
	)
}
