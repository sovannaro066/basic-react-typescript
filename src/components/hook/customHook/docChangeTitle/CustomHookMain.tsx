import { DocTitleOne } from "./DocTitleOne";
import { DocTitleTwo } from "./DocTitleTwo";

export const CustomHookMain = () => {
	return (
		<>
			<div><DocTitleOne/></div>
			<div><DocTitleTwo/></div>
		</>
	)
}
